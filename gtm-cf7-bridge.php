<?php
/*
Plugin Name: Google Tag Manager Contact Form 7 Bridge
Description: Setup a bridge Google Tag Manager et Contact Form 7
Author: Wooz'up - Victor ANTOINE
Version: 1.1
Author URI: https://www.wooz-up.com
*/

/**
 * Load the js script
 *
 * @return void
 */
function gtm_cf7_bridge_enqueue_script() {
    wp_enqueue_script('gtm-cf7-bridge', plugin_dir_url( __FILE__ ) . '/js/gtm-cf7-bridge.js', null, null, true);
}
add_action( 'wp_enqueue_scripts', 'gtm_cf7_bridge_enqueue_script' );