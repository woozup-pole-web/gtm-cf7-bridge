# Google Tag Manager Contact Form 7 Bridge

This plugin triggers a dataLayer push of an event triggered by Contact Form 7 with all the properties in a parameter named _gtm-cf7-bridge_.

Contact Form 7 documentation : https://contactform7.com/dom-events/