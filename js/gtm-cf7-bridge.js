/**
 * Push custom events to the GTM DataLayer when CF7 events are triggered
 * https://contactform7.com/dom-events/
 */
class GtmCf7Bridge {

    /**
     * CF7 DOM events that are listened for.
     * https://contactform7.com/dom-events/
     * @type {[string, string, string, string, string]}
     */
    #trackedFormEvent = [
        'wpcf7invalid',
        'wpcf7spam',
        'wpcf7mailsent',
        'wpcf7mailfailed',
        'wpcf7submit'
    ];


    /**
     * Initialize the bridge between CF7 and GTM
     */
    init() {
        this.addEventListeners();
    }

    /**
     * Add the eventListeners to the trackedFormEvents
     */
    addEventListeners() {
        for (const eventType in this.#trackedFormEvent) {
            if (this.#trackedFormEvent.hasOwnProperty(eventType)) {
                document.addEventListener(this.#trackedFormEvent[eventType], GtmCf7Bridge.pushFormEventToDatalayer, false);
            }
        }
    }

    /**
     * Push a custom event to the DataLayer
     * @param event The event originating from an addEventListener and where the eventType is managed by CF7
     */
    static pushFormEventToDatalayer(event) {
        /**
         * @var dataLayer
         * Should be defined by any GTM implementation on the website
         */
        if (dataLayer !== undefined) {

            let cf7Data = [];
            event.detail.inputs.forEach(input => {
                cf7Data.push({
                    'name': input.name,
                    'value': input.value
                });
            });

            /**
             *
             * @type {{"gtm-cf7-bridge.pluginVersion": *, "gtm-cf7-bridge.contactFormLocale": *, event: string, "gtm-cf7-bridge.unitTag": *, "gtm-cf7-bridge.containerPostId": *, "gtm-cf7-bridge.contactFormId": *}}
             * All the bridge gtm-cf7-bridge properties are defined by CF7
             * https://contactform7.com/dom-events/
             */
            let dataToPush = {
                'event': 'gtm-cf7-bridge.' + event.type,
                'gtm-cf7-bridge.contactFormId': event.detail.contactFormId,
                'gtm-cf7-bridge.pluginVersion': event.detail.pluginVersion,
                'gtm-cf7-bridge.contactFormLocale': event.detail.contactFormLocale,
                'gtm-cf7-bridge.unitTag': event.detail.unitTag,
                'gtm-cf7-bridge.containerPostId': event.detail.containerPostId,
                'gtm-cf7-bridge.cf7-data': cf7Data,
            };

            dataLayer.push(dataToPush);
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    let gtmCf7Bridge = new GtmCf7Bridge();
    gtmCf7Bridge.init();
});